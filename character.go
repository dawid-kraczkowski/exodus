package exodus

import "exodus/character"

type Trait struct {

}

type Perk struct {

}

type Character struct {
	Race character.Race
	Strength character.Strength
	Perception character.Perception
	Endurance character.Endurance
	Charisma character.Charisma
	Intelligence character.Intelligence
	Agility character.Agility
	Luck character.Luck
	Armor character.Armor
	Karma character.Karma
	Skills []character.Skill
	SkillTriggers []SkillTrigger
	LevelUpTriggers []LevelUpTrigger
}

type SkillTrigger func(skill character.SkillName, player Character) Character
type LevelUpTrigger func(level uint8, player Character) Character

func (character *Character) UseWeapon(weapon Weapon, target *Character) {

}

type PrimaryStatistics struct {
	Strength character.Strength
	Perception character.Perception
	Endurance character.Endurance
	Charisma character.Charisma
	Intelligence character.Intelligence
	Agility character.Agility
	Luck character.Luck
}

func NewSuperMutant(primaryStatistics PrimaryStatistics) Character {
	return Character {
		Race: character.Races.SuperMutant,
		Strength: primaryStatistics.Strength,
		Perception: primaryStatistics.Perception,
		Endurance: primaryStatistics.Endurance,
		Charisma: primaryStatistics.Charisma,
		Intelligence: primaryStatistics.Intelligence,
		Agility: primaryStatistics.Agility,
		Luck: primaryStatistics.Luck,
		SkillTriggers: []SkillTrigger{
			// Small guns penalty -2 to perception
			func(skill character.SkillName, player Character) Character {
				if skill == character.Skills.SmallGuns {
					player.Perception -= 2
				}

				return player
			},
		},
		LevelUpTriggers: []LevelUpTrigger{

		},
	}
}