package exodus

import "testing"

func TestRollADice(test *testing.T) {
	for i := 0; i < 100; i++ {
		result := D2.Roll(1)
		if result != 1 && result != 2 {
			test.Error("Dice result should be 1 or 2, got:", result)
		}
	}
}