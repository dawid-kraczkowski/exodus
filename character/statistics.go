package character

type Statistic uint8

type PrimaryStatistic Statistic

type Strength PrimaryStatistic

type Perception PrimaryStatistic

type Endurance PrimaryStatistic

type Charisma PrimaryStatistic

type Intelligence PrimaryStatistic

type Agility PrimaryStatistic

type Luck PrimaryStatistic

type BaseStrength struct {
	Minimum Strength
	Maximum Strength
}

type BasePerception struct {
	Minimum Perception
	Maximum Perception
}

type BaseEndurance struct {
	Minimum Endurance
	Maximum Endurance
}

type BaseCharisma struct {
	Minimum Charisma
	Maximum Charisma
}

type BaseIntelligence struct {
	Minimum Intelligence
	Maximum Intelligence
}

type BaseAgility struct {
	Minimum Agility
	Maximum Agility
}

type BaseLuck struct {
	Minimum Luck
	Maximum Luck
}

type SecondaryStatistic Statistic

type Karma SecondaryStatistic

type ArmorClass SecondaryStatistic

type RadiationResistance SecondaryStatistic

type PoisonResistance SecondaryStatistic

type DamageResistance SecondaryStatistic

type ElectricityResistance SecondaryStatistic

type Armor struct {
	ArmorClass ArmorClass
	RadiationResistance RadiationResistance
	PoisonResistance PoisonResistance
	DamageResistance DamageResistance
	ElectricityResistance ElectricityResistance
}