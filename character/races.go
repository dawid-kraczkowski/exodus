package character

type raceEnum struct {
	Human Race
	Ghoul Race
	SuperMutant Race
}

type PrimaryStatisticContainer interface {
	GetStrength() Strength
	GetPerception() Perception
	GetEndurance() Endurance
}

type Race struct {
	Strength BaseStrength
	Perception BasePerception
	Endurance BaseEndurance
	Charisma BaseCharisma
	Intelligence BaseIntelligence
	Agility BaseAgility
	Luck BaseLuck

	PerkOffset int8
	Karma Karma
	ArmorClass ArmorClass
	RadiationResistance RadiationResistance
	PoisonResistance PoisonResistance
	DamageResistance DamageResistance
	ElectricityResistance ElectricityResistance
}

var Races = func() *raceEnum {
	return &raceEnum {
		Human: Race {
			Strength: BaseStrength {1, 10},
			Perception: BasePerception {1, 10},
			Endurance: BaseEndurance {1, 10},
			Charisma: BaseCharisma {1, 10},
			Intelligence: BaseIntelligence {1, 10},
			Agility: BaseAgility {1, 10},
			Luck: BaseLuck {1, 10},
			PerkOffset: 3,
			ElectricityResistance: 30,
		},
		Ghoul: Race {
			Strength: BaseStrength {1, 8},
			Perception: BasePerception {4, 13},
			Endurance: BaseEndurance {1, 10},
			Charisma: BaseCharisma {1, 10},
			Intelligence: BaseIntelligence {2, 10},
			Agility: BaseAgility {1, 6},
			Luck: BaseLuck {5, 12},
			PerkOffset: 4,
			RadiationResistance: 80,
			PoisonResistance: 30,
		},
		SuperMutant: Race {
			Strength: BaseStrength {1, 8},
			Perception: BasePerception {4, 13},
			Endurance: BaseEndurance {1, 10},
			Charisma: BaseCharisma {1, 10},
			Intelligence: BaseIntelligence {2, 10},
			Agility: BaseAgility {1, 6},
			Luck: BaseLuck {5, 12},
			PerkOffset: 4,
			RadiationResistance: 80,
			PoisonResistance: 30,
		},
	}
}()