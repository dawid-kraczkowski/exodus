package character

type SkillName string

type skillNameEnum struct {
	SmallGuns SkillName
	BigGuns SkillName
	EnergyWeapons SkillName
	Unarmed SkillName
	MeleeWeapons SkillName
	Throwing SkillName
	FirstAid SkillName
	Doctor SkillName
	Pilot SkillName
	Sneak SkillName
	Lockpick SkillName
	Steal SkillName
	Traps SkillName
	Science SkillName
	Repair SkillName
	Speech SkillName
	Barter SkillName
	Gambling SkillName
	Outdoorsman SkillName
}

var Skills = func() *skillNameEnum {
	return &skillNameEnum {
		SmallGuns: "Small Guns",
		BigGuns: "Big Guns",
		EnergyWeapons: "Energy Weapons",
		Unarmed: "Unarmed",
		MeleeWeapons: "Melee Weapons",
		Throwing: "Throwing",
		FirstAid: "FirstAid",
		Doctor: "Doctor",
		Pilot: "Pilot",
		Sneak: "Sneak",
		Lockpick: "Lockpick",
		Steal: "Steal",
		Traps: "Traps",
		Science: "Science",
		Repair: "Repair",
		Speech: "Speech",
		Barter: "Barter",
		Gambling: "Gambling",
		Outdoorsman: "Outdoorsman",
	}
}()

type Skill struct {
	Name SkillName
	Value int16
}

type Skilled interface {
	GetSkill(name SkillName) Skill
}


