package exodus

import "time"
import "math/rand"

type Dice int8

const (
	D2 Dice = 2
	D4 Dice = 4
	D6 Dice = 6
	D8 Dice = 8
	D10 Dice = 10
	D12 Dice = 12
)

func (dice Dice) Roll(times int) int {
	result := 0

	for i := 0; i < times; i++ {
		roll := rand.New(rand.NewSource(time.Now().UnixNano()))
		result += roll.Intn(int(dice)) + 1
	}

	return result
}